var http = require('http'), io = require('socket.io');
var User = require('./objects/user.js');
var users = [];
var server = http.createServer(function (req, res) {
	res.writeHead(200, { 'Content-Type': 'text/html' });
	res.end('<h1>Hello Socket Lover!</h1>');
});
var http = require('request');
server.listen(8080);

var socket = io.listen(server);
socket.on('connection', function (client) {

	client.on('message', function (data) {
		client.broadcast.to(data._headers.room).emit(data._headers.type, data);
		http.post('http://localhost:2222/messages/create', {
			form: {
				'author': data._headers.author,
				'body': data._body
			}
		})
	});

	client.on('login', function (data) {
		client.join('main');
		var user = new User.Class(data);
		user.sock_id = client.id;
		var userData = getUserListData(user);
		if (users.length > 0) {
			client.emit('userList', users);
		}
		users.push(userData);
		client.broadcast.emit('newUser', data);
		getMessageHistory(function(data){
			client.emit('messageHistory',data)
		});
	});

	client.on('disconnect', function (data) {
		for (var i = 0; i < users.length; i++) {
			if (users[i].sock_id == client.id) {
				var userToRemove = users[i];
				users.splice(i, 1);
			}
		}
		if (users.length > 0) {
			client.broadcast.emit('removeUser', userToRemove);
		}
	});
});

function getMessageHistory(callback) {
	http.get('http://localhost:2222/messages/index')
		.on('response', function (response) {
			var body = '';
			response.on('data', function (chunk) {
				body += chunk;
			});
			response.on('end', function () {
				//console.log(body);
				callback(body);
			});
		})
}

function getUserListData(user) {
	return {
		username: user.username,
		id: user.id,
		email: user.email,
		sock_id: user.sock_id
	};
}

function sendUserList(client, data) {

}

function merge(obj1, obj2) {
	for (var prop in obj1) {
		obj2[prop] = obj1[prop];
	}
	return obj2;
}